import uuid
import argparse

parser = argparse.ArgumentParser(description='Generate long output.')
parser.add_argument(
    '--output_size', '-o', type=float, default=1,
    help='how much output to generate in MiB'
)
args = parser.parse_args()

MiB = 2 ** 20
# size of one line with UUID is 37 ASCII charactes
for i in range(int(args.output_size * MiB // 37)):
    print(str(uuid.uuid4()))
